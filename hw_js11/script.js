
// У файлі index.html лежить розмітка двох полів вводу пароля.

// Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів
// користувач, іконка змінює свій зовнішній вигляд. У коментарях під іконкою - інша іконка,
// саме вона повинна відображатися замість поточної.

// Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)

// Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)

// Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях

// Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;

// Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення

// Після натискання на кнопку сторінка не повинна перезавантажуватись

// Можна міняти розмітку, додавати атрибути, теги, id, класи тощо.

let form = document.querySelector('.password-form');
let password = document.getElementById('password');
let password_confirm = document.getElementById('password_confirm');

let button = document.getElementsByTagName('button');
let error = document.querySelector('span');


form.addEventListener('click', function (event) {
    const currentElement = event.target;
    function opacityChange(opacityIndex) {
        document.getElementById("error").style.opacity = opacityIndex;
    };
    if (currentElement.classList.contains('button')) {
        if (password.value === password_confirm.value) {
            opacityChange(0);
            alert("You are welcome");
        } else {
            opacityChange(1)
        }
    }

    if (currentElement.classList.contains('icon-password')) {
        const dataType = currentElement.dataset.id
        const inputPass = document.getElementById(dataType)

            if (currentElement.classList.contains('fa-eye')) {
                currentElement.classList.add('fa-eye-slash')
                currentElement.classList.remove('fa-eye')
                inputPass.setAttribute('type', 'text');

            } else {
                currentElement.classList.add('fa-eye')
                currentElement.classList.remove('fa-eye-slash')
                inputPass.setAttribute('type', 'password');}
          }
      })
